Express template
===================

Generated base project structure for express application

Usage
-----

Install:

.. code:: bash

  $ npm i -g babel-cli
  $ npm i -g nodemon
  $ npm i -g webpack
  $ npm i

Run Server:

.. code:: bash

  $ npm start

Run Webpack:

.. code:: bash

  $ npm run front