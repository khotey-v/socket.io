import Error from '../models/error';

export default function (err, req, res, next) {
  Error.create(err);

  next();
};