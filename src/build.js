import io from 'socket.io-client';
import $ from 'jquery';

const socket = io('http://192.168.9.89:3001');

$('form').on('submit', ((e) => {
  e.preventDefault();

  socket.emit('chat message', $('#m').val());
  $('#m').val('');
}));

socket.on('chat message', (msg) => {
  $('#messages').append(`<li>${msg}</li>`);
});

socket.on('typing', (msg) => {
  console.log(`user typing: ${msg}`);
});

$('#m').on('keypress', (e) => {
  socket.emit('typing', $('#m').val());
});

$(window).mousemove((e) => {
  const cords = {x: 0, y: 0};

  cords.x = e.pageX;
  cords.y = e.pageY;

  socket.emit('mousemove', cords);
});

socket.on('mousemove', (cords) => {
  $('.dot').css({
    left: cords.x,
    top: cords.y
  });
});
