import express from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import ejsMate from 'ejs-mate';
import mongoose from 'mongoose';
import flash from 'express-flash';
import morgan from 'morgan';
import socket from 'socket.io';
import http from 'http';
import cors from 'cors';

import config from './config';
import mainRoute from './routes/main';
import errorLogger from './middlewares/errorLogger';

const app = express();
const server = http.Server(app);
const io = socket(server);

app.listen(config.port, (err) => {
  if (err) throw err;

  console.info(`Server's PORT:${config.port}`);
});

// REMOVE
if (config.database) {
  mongoose.connect(config.database, (err) => {
    if (err) throw err;

    console.info(`Success connect to mongodb:${config.database}`);
  });
}

app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.use(cors({
  origin: 'http://localhost:8080',
  optionsSuccessStatus: 200
}));
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.secretKey
}));
app.use(flash());

app.use('/public', express.static(__dirname + '/public'));
app.use(mainRoute);

server.listen(3001);

io.on('connection', function(socket){
    console.log('a user connected');

  socket.on('disconnect', () => {
    console.log('user disconect');
  });

  socket.on('chat message', (msg) => {
    io.emit('chat message', msg);
  });

  socket.on('typing', (msg) => {
    socket.broadcast.emit('typing', msg);
  });

  socket.on('mousemove', (cords) => {
    socket.broadcast.emit('mousemove', cords);
  });
});

// Error handler
app.use(errorLogger);
